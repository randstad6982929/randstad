package com.jsperales.randstad;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class RandstadApplicationTests {

	@Autowired
	private RandstadRest rest;

	@Autowired
	private MockMvc mockMvc;
	
	@Test
	void contextLoads() {
		assertThat(rest).isNotNull();
	}	

	@Test
	public void requestFourPMOnFourteenth() throws Exception {
		this.mockMvc.perform(get("/api/price/2020-06-14T16:00:00/35455/1"))
					.andDo(print())
					.andExpect(status().isOk());
	}
	
	@Test
	public void requestNinePMOnFourteenth() throws Exception {
		this.mockMvc.perform(get("/api/price/2020-06-14T21:00:00/35455/1"))
					.andDo(print())
					.andExpect(status().isOk());
	}
	
	@Test
	public void requestTenAMOnFifteenth() throws Exception {
		this.mockMvc.perform(get("/api/price/2020-06-14T10:00:00/35455/1"))
					.andDo(print())
					.andExpect(status().isOk());
	}
	
	@Test
	public void requestNinePMOnFifteenth() throws Exception {
		this.mockMvc.perform(get("/api/price/2020-06-15T21:00:00/35455/1"))
					.andDo(print())
					.andExpect(status().isOk());
	}
	
	@Test
	public void requestTenAMOnFourteenth() throws Exception {//fifteenth
		this.mockMvc.perform(get("/api/price/2020-06-15T10:00:00/35455/1"))
					.andDo(print())
					.andExpect(status().isOk());
	}
}
