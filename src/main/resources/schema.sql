DROP TABLE PRICES;
CREATE TABLE PRICES ( 
   ID INT NOT NULL, 
   BRAND_ID INT NOT NULL, 
   START_DATE TIMESTAMP NOT NULL, 
   END_DATE TIMESTAMP NOT NULL, 
   PRICE_LIST INT NOT NULL, 
   PRODUCT_ID INT NOT NULL,
   PRIORITY INT NOT NULL, 
   PRICE NUMERIC(20,2) NOT NULL,  
   CURR VARCHAR(20) NOT NULL
);