package com.jsperales.randstad;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories("com.jsperales.randstad.persistance.repo") 
@EntityScan("com.jsperales.randstad.persistance.model")
@SpringBootApplication
public class RandstadApplication {

	public static void main(String[] args) {
		SpringApplication.run(RandstadApplication.class, args);
	}

}
