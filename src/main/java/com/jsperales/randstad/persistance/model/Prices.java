package com.jsperales.randstad.persistance.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonProperty;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.jackson.Jacksonized;

@Jacksonized
@Entity
@AllArgsConstructor
@Data
@NoArgsConstructor
@Table(name = "PRICES")
public class Prices {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonProperty("id")
    private long id;

    @Column(name = "BRAND_ID")
    @JsonProperty("idBrand")
    private Long idBrand;

    @Column(name = "START_DATE")
    @JsonProperty("startDate")
    private LocalDateTime startDate;
    
    @Column(name = "END_DATE")
    @JsonProperty("endDate")
    private LocalDateTime endDate;
    
    @Column(name = "PRICE_LIST")
    @JsonProperty("priceList")
    private Long priceList;
    
    @Column(name = "PRODUCT_ID")
    @JsonProperty("idProduct")
    private Long idProduct;

    @Column(name = "PRIORITY")
    @JsonProperty("priority")
    private Long priority;
    
    @Column(name = "PRICE")
    @JsonProperty("price")
    private BigDecimal price;
    
    @Column(name = "CURR")
    @JsonProperty("curr")
    private String curr;
}