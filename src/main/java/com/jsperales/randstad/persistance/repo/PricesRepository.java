package com.jsperales.randstad.persistance.repo;

import java.time.LocalDateTime;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.jsperales.randstad.persistance.model.Prices;

@Repository
public interface PricesRepository extends CrudRepository<Prices, Long> {
    Prices findFirstByStartDateBeforeAndEndDateAfterAndIdProductAndIdBrandOrderByPriorityDesc(LocalDateTime startDate,LocalDateTime endDate, Long idProduct, Long idBrand);
}