package com.jsperales.randstad;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jsperales.randstad.persistance.model.Prices;
import com.jsperales.randstad.persistance.repo.PricesRepository;

@RestController
@RequestMapping("/api")
public class RandstadRest {

	@Autowired
    private PricesRepository pricesRepository;

	
	@GetMapping("/allPrices")
    public Iterable<Prices> findAll() {
		return pricesRepository.findAll();
    }
	
	@GetMapping("/price/{date}/{idProduct}/{idBrand}")
    public Prices findByTitle(@PathVariable String date, @PathVariable Long idProduct, @PathVariable Long idBrand) {
        // TODO: I didnt understand what its the functionality of this method . 
		// Will change the repo and filters when explained
		return pricesRepository.findFirstByStartDateBeforeAndEndDateAfterAndIdProductAndIdBrandOrderByPriorityDesc(LocalDateTime.parse(date),LocalDateTime.parse(date), idProduct, idBrand);
    }
	
}
